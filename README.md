Prog Image
=========

## Overview

- Services built using Sinatra
- Each service is hosted in a Docker container
- Containers managed using Docker Compose
- Containers communicate via HTTP
- nginx container acting as a reverse proxy gateway for public access
- Images are persisted to S3
- Image manipulation uses ImageMagick

## Services

#### compression_service

Compress images

#### image_service

Download/upload images

#### nginx

nginx reverse proxy

#### thumbnail_service

Create image thumbnails

## Usage

The application services can be launched by running `docker-compose up`

## Tests

Services can be tested invididually by running `rake test`

## API Client

A very simple API client exists in `node_module`
