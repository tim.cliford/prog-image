require 'minitest/autorun'

require_relative '../../../app/helpers/thumbnail_helper'

class TestThumbnailHelper < Minitest::Test

  def setup
    @thumbnail_helper = ThumbnailService::Helpers::ThumbnailHelper.new
  end

  def test_create_thumbnail_creates_thumbnail
    image = MiniMagick::Image.open(TestHelpers.test_image)
    image_blob = image.to_blob

    compressed_blob = @thumbnail_helper.create_thumbnail(image_blob, '50%')

    refute_nil(compressed_blob)
    assert(compressed_blob.length < image_blob.length)
  end

  def test_content_type_valid_returns_true_for_valid_content_types
    valid_extensions = %w(image/jpeg image/png image/gif image/bmp)

    valid_extensions.each { |x| assert_equal(true, @thumbnail_helper.content_type_valid?(x)) }
  end

  def test_content_type_valid_returns_false_for_invalid_content_types
    invalid_extensions = %w(image/bla application/json application/text)

    invalid_extensions.each { |x| assert_equal(false, @thumbnail_helper.content_type_valid?(x)) }
  end

end