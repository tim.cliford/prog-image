require 'minitest/autorun'
require 'rack/test'

require_relative '../../../app'
require_relative '../../../app/routes/thumbnail_routes'

class TestThumbnailRoutes < Minitest::Test
  include Rack::Test::Methods

  def app
    ThumbnailService::App
  end

  def test_compress_blob_does_compress_for_valid_parameters
    image_path = File.join(File.dirname(__FILE__), '../../cavoodle.jpeg')
    blob = Rack::Test::UploadedFile.new(image_path, 'image/jpeg')

    post '/thumbnail-blob?geometry=50%25', blob, 'CONTENT_TYPE' => 'image/jpeg'

    assert_equal(200, last_response.status)
    assert(last_response.body.length < blob.length)
  end

  def test_compress_blob_returns_400_for_missing_quality
    image_path = File.join(File.dirname(__FILE__), '../../cavoodle.jpeg')
    blob = Rack::Test::UploadedFile.new(image_path, 'image/jpeg')

    post '/thumbnail-blob', blob, 'CONTENT_TYPE' => 'image/jpeg'

    assert_equal(400, last_response.status)
  end

  def test_compress_blob_returns_400_for_missing_image=
    post '/thumbnail-blob?geometry=50%25', {}, 'CONTENT_TYPE' => 'image/jpeg'

    assert_equal(400, last_response.status)
  end

  def test_thumbnail_id_does_thumbnail_for_valid_parameters
    post '/thumbnail-id?id=cavoodle&geometry=50%25', {}, 'CONTENT_TYPE' => 'image/jpeg'

    assert_equal(200, last_response.status)
  end

  def test_thumbnail_id_returns_400_for_missing_id
    post '/thumbnail-id?geometry=50%25', {}, 'CONTENT_TYPE' => 'image/jpeg'

    assert_equal(400, last_response.status)
  end

  def test_thumbnail_id_returns_400_for_missing_quality
    post '/thumbnail-blob?id=cavoodle', {}, 'CONTENT_TYPE' => 'image/jpeg'

    assert_equal(400, last_response.status)
  end

  def test_thumbnail_url_does_create_thumbnail_for_valid_parameters
    url = 'http%3A%2F%2Fpets.petsmart.com%2Ffood-center%2Fassets%2F_images%2F_article-choosing-the-right-puppy-food%2Fimg-puppy-running.jpg'

    post "/thumbnail-url?url=#{url}&geometry=50%25", {}, 'CONTENT_TYPE' => 'image/jpeg'

    assert_equal(200, last_response.status)
  end

  def test_thumbnail_url_returns_400_for_missing_url
    post '/thumbnail-url?geometry=50%25', {}, 'CONTENT_TYPE' => 'image/jpeg'

    assert_equal(400, last_response.status)
  end

  def test_thumbnail_url_returns_400_for_missing_quality
    url = 'http%3A%2F%2Fpets.petsmart.com%2Ffood-center%2Fassets%2F_images%2F_article-choosing-the-right-puppy-food%2Fimg-puppy-running.jpg'

    post "/thumbnail-url?url=#{url}", {}, 'CONTENT_TYPE' => 'image/jpeg'

    assert_equal(400, last_response.status)
  end

end
