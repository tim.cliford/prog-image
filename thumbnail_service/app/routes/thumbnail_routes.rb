require 'open-uri'
require 'uri'

require_relative '../../app/helpers/thumbnail_helper'

module ThumbnailService
  module Routes
    class ThumbnailRoutes < Sinatra::Application

      configure do
        mime_type :jpeg, 'image/jpeg'
        mime_type :png, 'image/png'
        mime_type :gif, 'image/gif'
        mime_type :bmp, 'image/bmp'
      end

      before do
        @thumbnail_helper = ThumbnailService::Helpers::ThumbnailHelper.new
      end

      post '/thumbnail-blob' do
        halt(400, 'Missing image') if request.content_length == '0'
        halt(400, 'Missing geometry parameter') if params[:geometry].nil?
        halt(400, 'Invalid content_type') unless @thumbnail_helper.content_type_valid?(request.content_type)
        blob = @thumbnail_helper.create_thumbnail(request.body.read, params[:geometry])
        halt(500, 'Image compression failed') if blob.nil?
        content_type request.content_type
        blob
      end

      post '/thumbnail-id' do
        halt(400, 'Missing id parameter') if params[:id].nil?
        halt(400, 'Missing geometry parameter') if params[:geometry].nil?
        uri = URI.join(ENV['IMAGE_SERVICE_DOWNLOAD'], params[:id] + '.jpeg')
        download_then_create_thumbnail(uri, params[:geometry])
      end

      post '/thumbnail-url' do
        halt(400, 'Missing url parameter') if params[:url].nil?
        halt(400, 'Missing geometry parameter') if params[:geometry].nil?
        download_then_create_thumbnail(params[:url], params[:geometry])
      end

      def download_then_create_thumbnail(url, geometry)
        begin
          response = open(url)
          halt(400, 'Invalid image format') unless @thumbnail_helper.content_type_valid?(response.content_type)
        rescue StandardError => e
          halt(500, "Error downloading URL: #{e}")
        end
        blob = @thumbnail_helper.create_thumbnail(response.read, geometry)
        halt(500, 'Image thumbnail failed') if blob.nil?
        content_type response.content_type
        blob
      end

    end
  end
end