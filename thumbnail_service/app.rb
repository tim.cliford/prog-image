require 'bundler/setup'
Bundler.setup

require 'dotenv'
Dotenv.load

require 'sinatra/base'

require_relative 'app/routes/thumbnail_routes'

module ThumbnailService
  class App < Sinatra::Application
    configure do
      disable :method_override
      disable :static
    end

    use ThumbnailService::Routes::ThumbnailRoutes
  end
end