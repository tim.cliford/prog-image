require 'aws-sdk'

module ImageService
  module Helpers
    class S3Helper

      def initialize
        @s3 = Aws::S3::Resource.new(region: ENV['AWS_REGION'])
      end

      def download(name)
        bucket = @s3.bucket(ENV['AWS_S3_BUCKET'])
        return nil unless bucket.exists?
        object = bucket.object(name)
        return nil unless object.exists?
        object.get()
      end

      def upload(blob, content_type)
        bucket = @s3.bucket(ENV['AWS_S3_BUCKET'])
        return nil unless bucket.exists?
        bucket.put_object({
          acl: 'public-read',
          key: generate_object_key(24),
          body: blob,
          content_type: content_type
        })
      end

      def generate_object_key(length)
        charset = Array('A'..'Z') + Array('a'..'z')
        Array.new(length) { charset.sample }.join
      end

    end
  end
end