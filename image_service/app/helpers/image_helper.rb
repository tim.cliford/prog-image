require 'mini_magick'

module ImageService
  module Helpers
    class ImageHelper

      def convert(blob, format)
        begin
          image = MiniMagick::Image.read(blob)
          image.format format
          return image.to_blob
        rescue MiniMagick::Invalid
          return nil
        end
      end

      def needs_conversion?(current, requested)
        !(current.include? requested)
      end

      def extension_valid?(extension)
        if extension.nil? || extension.length == 0
          return false
        end
        %w(jpeg png gif bmp).include? extension.downcase
      end

      def content_type_valid?(content_type)
        if content_type.nil? || content_type.length == 0
          return false
        end
        %w(image/jpeg image/png image/gif image/bmp).include? content_type.downcase
      end

    end
  end
end