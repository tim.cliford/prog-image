require 'sinatra/base'

require_relative '../../app/helpers/s3_helper'
require_relative '../../app/helpers/image_helper'

module ImageService
  module Routes
    class ImageRoutes < Sinatra::Application

      configure do
        mime_type :jpeg, 'image/jpeg'
        mime_type :png, 'image/png'
        mime_type :gif, 'image/gif'
        mime_type :bmp, 'image/bmp'
      end

      before do
        @s3_helper = ImageService::Helpers::S3Helper.new
        @image_helper = ImageService::Helpers::ImageHelper.new
      end

      get '/download/*.*' do |id, extension|
        halt(400, "#{extension} is not a valid extension") unless @image_helper.extension_valid?(extension)
        object = @s3_helper.download(id)
        halt(404, "Image '#{id}' does not exist") if object.nil?
        blob = object.body
        if @image_helper.needs_conversion?(object.content_type, extension)
          blob = @image_helper.convert(blob, extension)
          halt(500, 'Image conversion error') if blob.nil?
        end
        content_type extension
        blob
      end

      post '/upload' do
        halt(400, 'Missing image') if request.content_length == '0'
        halt(400, 'Invalid content_type') unless @image_helper.content_type_valid?(request.content_type)
        object = @s3_helper.upload(request.body.read, request.content_type)
        object.key
      end

    end
  end
end