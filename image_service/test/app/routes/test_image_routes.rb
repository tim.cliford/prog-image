require 'minitest/autorun'
require 'rack/test'
require 'aws-sdk'

require_relative '../../../app'
require_relative '../../../app/routes/image_routes'

class TestImageRoutes < Minitest::Test
  include Rack::Test::Methods

  def app
    ImageService::App
  end

  def setup
    Aws.config[:stub_responses] = true
  end

  def test_download_returns_200_for_valid_id_and_extension
    Aws.config[:s3] = {
      stub_responses: {
        get_object: { body: 'some data', content_type: 'image/jpeg' }
      }
    }

    get '/download/abc.jpeg'

    assert_equal(200, last_response.status)
  end

  def test_download_returns_404_for_invalid_id
    Aws.config[:s3] = {
      stub_responses: {
        head_object: { status_code: 404, headers: {}, body: '' }
      }
    }

    get '/download/abc.jpeg'

    assert_equal(404, last_response.status)
  end

  def test_upload_returns_200_for_valid_input
    post '/upload', Rack::Test::UploadedFile.new(TestHelpers.test_image, 'image/jpeg'), 'CONTENT_TYPE' => 'image/jpeg'

    assert_equal(200, last_response.status)
  end

  def test_upload_returns_400_for_invalid_input
    post '/upload', {}, 'CONTENT_TYPE' => 'image/jpeg'

    assert_equal(400, last_response.status)
  end

  def test_upload_returns_400_for_invalid_content_type
    post '/upload', Rack::Test::UploadedFile.new(TestHelpers.test_image, 'image/invalid'), 'CONTENT_TYPE' => 'image/invalid'

    assert_equal(400, last_response.status)
  end

end