require 'minitest/autorun'
require 'aws-sdk'
require 'mini_magick'

require_relative '../../../app/helpers/s3_helper'

require 'dotenv'
Dotenv.load

class TestS3Helper < Minitest::Test

  def setup
    Aws.config[:stub_responses] = true
  end

  def test_download_returns_image_when_object_does_exist
    Aws.config[:s3] = {
      stub_responses: {
        get_object: { body: 'some data' }
      }
    }
    s3_helper = ImageService::Helpers::S3Helper.new

    object = s3_helper.download('id')

    refute_nil(object)
    assert_equal('some data', object.body.string)
  end

  def test_download_returns_nil_when_obect_does_not_exist
    Aws.config[:s3] = {
      stub_responses: {
        head_object: { status_code: 404, headers: {}, body: '' }
      }
    }
    s3_helper = ImageService::Helpers::S3Helper.new

    object = s3_helper.download('id')

    assert_nil(object)
  end

  def test_upload_returns_object_for_valid_input
    s3_helper = ImageService::Helpers::S3Helper.new
    image = MiniMagick::Image.open(TestHelpers.test_image)
    content_type = 'image/jpeg'

    object = s3_helper.upload(image.to_blob, content_type)

    refute_nil(object)
    assert_kind_of String, object.key
  end

  def test_generate_object_key_returns_key_of_correct_type_and_length
    s3_helper = ImageService::Helpers::S3Helper.new
    key = s3_helper.generate_object_key(50)

    refute_nil(key)
    assert_kind_of String, key
    assert_equal(50, key.length)
  end

end