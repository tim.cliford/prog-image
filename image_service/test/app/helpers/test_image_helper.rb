require 'minitest/autorun'

require_relative '../../../app/helpers/image_helper'

class TestImageHelper < Minitest::Test

  def setup
    @image_helper = ImageService::Helpers::ImageHelper.new
  end

  def test_needs_conversion_returns_true_when_current_does_not_match_requested
    current = 'image/jpeg'
    requested = 'png'

    assert_equal(true, @image_helper.needs_conversion?(current, requested))
  end

  def test_needs_conversion_returns_false_when_current_does_match_requested
    current = 'image/jpeg'
    requested = 'jpeg'

    assert_equal(false, @image_helper.needs_conversion?(current, requested))
  end

  def test_extension_valid_returns_true_for_valid_extensions
    valid_extensions = %w(jpeg png gif bmp JPEG PNG GIF BMP)

    valid_extensions.each { |x| assert_equal(true, @image_helper.extension_valid?(x)) }
  end

  def test_extension_valid_returns_false_for_invalid_extensions
    invalid_extensions = %w(jpg txt html)

    invalid_extensions.each { |x| assert_equal(false, @image_helper.extension_valid?(x)) }
  end

  def test_content_type_valid_returns_true_for_valid_content_types
    valid_extensions = %w(image/jpeg image/png image/gif image/bmp)

    valid_extensions.each { |x| assert_equal(true, @image_helper.content_type_valid?(x)) }
  end

  def test_content_type_valid_returns_false_for_invalid_content_types
    invalid_extensions = %w(image/bla application/json application/text)

    invalid_extensions.each { |x| assert_equal(false, @image_helper.content_type_valid?(x)) }
  end

end