require 'bundler/setup'
Bundler.setup

require 'dotenv'
Dotenv.load

require 'sinatra/base'

require_relative 'app/routes/image_routes'

module ImageService
  class App < Sinatra::Application
    configure do
      disable :method_override
      disable :static
    end

    use ImageService::Routes::ImageRoutes
  end
end