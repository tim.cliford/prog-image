require 'open-uri'
require 'uri'

require_relative '../../app/helpers/compression_helper'

module CompressionService
  module Routes
    class CompressRoutes < Sinatra::Application

      configure do
        mime_type :jpeg, 'image/jpeg'
        mime_type :png, 'image/png'
        mime_type :gif, 'image/gif'
        mime_type :bmp, 'image/bmp'
      end

      before do
        @compression_helper = CompressionService::Helpers::CompressionHelper.new
      end

      post '/compress-blob' do
        halt(400, 'Missing image') if request.content_length == '0'
        halt(400, 'Missing quality parameter') if params[:quality].nil?
        halt(400, 'Invalid content_type') unless @compression_helper.content_type_valid?(request.content_type)
        blob = @compression_helper.compress(request.body.read, params[:quality])
        halt(500, 'Image compression failed') if blob.nil?
        content_type request.content_type
        blob
      end

      post '/compress-id' do
        halt(400, 'Missing id parameter') if params[:id].nil?
        halt(400, 'Missing quality parameter') if params[:quality].nil?
        uri = URI.join(ENV['IMAGE_SERVICE_DOWNLOAD'], params[:id] + '.jpeg')
        download_then_compress(uri, params[:quality])
      end

      post '/compress-url' do
        halt(400, 'Missing url parameter') if params[:url].nil?
        halt(400, 'Missing quality parameter') if params[:quality].nil?
        download_then_compress(params[:url], params[:quality])
      end

      def download_then_compress(url, quality)
        begin
          response = open(url)
          halt(400, 'Invalid image format') unless @compression_helper.content_type_valid?(response.content_type)
        rescue StandardError => e
          halt(500, "Error downloading URL: #{e}")
        end
        blob = @compression_helper.compress(response.read, quality)
        halt(500, 'Image compression failed') if blob.nil?
        content_type response.content_type
        blob
      end

    end
  end
end