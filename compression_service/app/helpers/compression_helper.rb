require 'mini_magick'

module CompressionService
  module Helpers
    class CompressionHelper

      def compress(blob, quality)
        begin
          image = MiniMagick::Image.read(blob)
          image.quality quality
          return image.to_blob
        rescue MiniMagick::Invalid
          return nil
        end
      end

      def content_type_valid?(content_type)
        if content_type.nil? || content_type.length == 0
          return false
        end
        %w(image/jpeg image/png image/gif image/bmp).include? content_type.downcase
      end

    end
  end
end