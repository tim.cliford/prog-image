require 'minitest/autorun'
require 'rack/test'

require_relative '../../../app'
require_relative '../../../app/routes/compress_routes'

class TestCompressRoutes < Minitest::Test
  include Rack::Test::Methods

  def app
    CompressionService::App
  end

  def test_compress_blob_does_compress_for_valid_parameters
    image_path = File.join(File.dirname(__FILE__), '../../cavoodle.jpeg')
    blob = Rack::Test::UploadedFile.new(image_path, 'image/jpeg')

    post '/compress-blob?quality=50', blob, 'CONTENT_TYPE' => 'image/jpeg'

    assert_equal(200, last_response.status)
    assert(last_response.body.length < blob.length)
  end

  def test_compress_blob_returns_400_for_missing_quality
    image_path = File.join(File.dirname(__FILE__), '../../cavoodle.jpeg')
    blob = Rack::Test::UploadedFile.new(image_path, 'image/jpeg')

    post '/compress-blob', blob, 'CONTENT_TYPE' => 'image/jpeg'

    assert_equal(400, last_response.status)
  end

  def test_compress_blob_returns_400_for_missing_image=
    post '/compress-blob?quality=50', {}, 'CONTENT_TYPE' => 'image/jpeg'

    assert_equal(400, last_response.status)
  end

  def test_compress_id_does_compress_for_valid_parameters
    post '/compress-id?id=cavoodle&quality=50', {}, 'CONTENT_TYPE' => 'image/jpeg'

    assert_equal(200, last_response.status)
  end

  def test_compress_id_returns_400_for_missing_id
    post '/compress-id?quality=50', {}, 'CONTENT_TYPE' => 'image/jpeg'

    assert_equal(400, last_response.status)
  end

  def test_compress_id_returns_400_for_missing_quality
    post '/compress-id?id=cavoodle', {}, 'CONTENT_TYPE' => 'image/jpeg'

    assert_equal(400, last_response.status)
  end

  def test_compress_url_does_compress_for_valid_parameters
    url = 'http%3A%2F%2Fpets.petsmart.com%2Ffood-center%2Fassets%2F_images%2F_article-choosing-the-right-puppy-food%2Fimg-puppy-running.jpg'

    post "/compress-url?url=#{url}&quality=50", {}, 'CONTENT_TYPE' => 'image/jpeg'

    assert_equal(200, last_response.status)
  end

  def test_compress_url_returns_400_for_missing_url
    post '/compress-url?quality=50', {}, 'CONTENT_TYPE' => 'image/jpeg'

    assert_equal(400, last_response.status)
  end

  def test_compress_url_returns_400_for_missing_quality
    url = 'http%3A%2F%2Fpets.petsmart.com%2Ffood-center%2Fassets%2F_images%2F_article-choosing-the-right-puppy-food%2Fimg-puppy-running.jpg'

    post "/compress-url?url=#{url}", {}, 'CONTENT_TYPE' => 'image/jpeg'

    assert_equal(400, last_response.status)
  end

end
