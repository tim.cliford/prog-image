require 'bundler/setup'
Bundler.setup

require 'dotenv'
Dotenv.load

require 'sinatra/base'

require_relative 'app/routes/compress_routes'

module CompressionService
  class App < Sinatra::Application
    configure do
      disable :method_override
      disable :static
    end

    use CompressionService::Routes::CompressRoutes
  end
end