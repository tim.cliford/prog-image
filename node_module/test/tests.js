'use strict';

const fs = require('fs');
const path = require('path');
const chai = require('chai');
const expect = chai.expect;
const chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const progImage = require('../index.js')
const client = new progImage({ host: 'http://localhost:8080/' });

let loadPuppy = function() {
  return fs.readFileSync(path.join(__dirname, './cavoodle.jpeg'));
};

describe('download', function() {
  it('downloads an image', () => {
    let result = client.download('cavoodle', 'jpeg');
    return Promise.all([
      expect(result.then(r => r.body)).to.eventually.have.length.of.at.least(1),
      expect(result.then(r => r.statusCode)).to.eventually.equal(200)
    ]);
  });
});

describe('upload', function() {
  it('uploads an image', () => {
    let puppy = loadPuppy();
    let result = client.upload(puppy, 'image/jpeg');
    return Promise.all([
      expect(result.then(r => r.body)).to.eventually.have.length.of.at.least(1),
      expect(result.then(r => r.statusCode)).to.eventually.equal(200)
    ]);
  });
});

describe('compressBlob', function() {
  it('compress an image blob', () => {
    let puppy = loadPuppy();
    let result = client.compressBlob(puppy, 'image/jpeg', 50);
    return Promise.all([
      expect(result.then(r => r.body)).to.eventually.have.length.of.at.least(1),
      expect(result.then(r => r.statusCode)).to.eventually.equal(200)
    ]);
  });
});

describe('compressId', function() {
  it('compress an image by ID', () => {
    let result = client.compressId('cavoodle', 50);
    return Promise.all([
      expect(result.then(r => r.body)).to.eventually.have.length.of.at.least(1),
      expect(result.then(r => r.statusCode)).to.eventually.equal(200)
    ]);
  });
});

describe('compressUrl', function() {
  it('compress an image by URL', () => {
    let result = client.compressUrl('http://ghk.h-cdn.co/assets/16/09/980x490/landscape-1457107485-gettyimages-512366437.jpg', 50);
    return Promise.all([
      expect(result.then(r => r.body)).to.eventually.have.length.of.at.least(1),
      expect(result.then(r => r.statusCode)).to.eventually.equal(200)
    ]);
  });
});

describe('thumbnailBlob', function() {
  it('create thumbnail of an image blob', () => {
    let puppy = loadPuppy();
    let result = client.thumbnailBlob(puppy, 'image/jpeg', '50%');
    return Promise.all([
      expect(result.then(r => r.body)).to.eventually.have.length.of.at.least(1),
      expect(result.then(r => r.statusCode)).to.eventually.equal(200)
    ]);
  });
});

describe('thumbnailId', function() {
  it('create thumbnail of an image by ID', () => {
    let result = client.thumbnailId('cavoodle', '50%');
    return Promise.all([
      expect(result.then(r => r.body)).to.eventually.have.length.of.at.least(1),
      expect(result.then(r => r.statusCode)).to.eventually.equal(200)
    ]);
  });
});

describe('thumbnailUrl', function() {
  it('create thumbnail of an image by URL', () => {
    let result = client.thumbnailUrl('http://ghk.h-cdn.co/assets/16/09/980x490/landscape-1457107485-gettyimages-512366437.jpg', '50%');
    return Promise.all([
      expect(result.then(r => r.body)).to.eventually.have.length.of.at.least(1),
      expect(result.then(r => r.statusCode)).to.eventually.equal(200)
    ]);
  });
});
