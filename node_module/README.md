Prog Image Client
=========

A client for the Prog Image API

## Installation

  `npm install prog-image-api`

## Usage

    // Create client
    let progImage = require('prog-image-client');
    let client = new progImage('http://localhost:8080/');

    // Get image
    client.download(id, extension).then(function(response) {
      // Do work
    }, function(error) {
      // Oops
    });

    // Upload image
    client.upload(blob, contentType).then(function(response) {
      // Do work
    }, function(error) {
      // Oops
    });

    // Compress raw image
    client.compressBlob(blob, contentType, quality).then(function(response) {
      // Do work
    }, function(error) {
      // Oops
    });

    // Compress image by ID
    client.compressId(id, quality).then(function(response) {
      // Do work
    }, function(error) {
      // Oops
    });

    // Compress image by URL
    client.compressUrl(url, quality).then(function(response) {
      // Do work
    }, function(error) {
      // Oops
    });

    // Thumbnail raw image
    client.thumbnailBlob(blob, contentType, geometry).then(function(response) {
      // Do work
    }, function(error) {
      // Oops
    });

    // Thumbnail image by ID
    client.thumbnailId(id, geometry).then(function(response) {
      // Do work
    }, function(error) {
      // Oops
    });

    // Thumbnail image by URL
    client.thumbnailUrl(url, geometry).then(function(response) {
      // Do work
    }, function(error) {
      // Oops
    });

## Tests

  `npm test`
