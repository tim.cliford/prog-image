'use strict';

const request = require('request');

let host = 'http://localhost:8080/';

let download = function(id, extension) {
  return new Promise((resolve, reject) => {
    request({
      method: 'GET',
      uri: `${host}download/${id}.${extension}`
    }, function (error, response, body) {
      if (error) {
        reject(error);
      } else {
        resolve(response);
      }
    });
  });
};

let upload = function(blob, contentType) {
  return new Promise((resolve, reject) => {
    request({
      method: 'POST',
      uri: `${host}upload`,
      body: blob,
      headers: {
        'content-type': contentType
      }
    }, function (error, response, body) {
      if (error) {
        reject(error);
      } else {
        resolve(response);
      }
    });
  });
};

let compressBlob = function(blob, contentType, quality) {
  return new Promise((resolve, reject) => {
    request({
      method: 'POST',
      uri: `${host}compress-blob?quality=${quality}`,
      body: blob,
      headers: {
        'content-type': contentType
      }
    }, function (error, response, body) {
      if (error) {
        reject(error);
      } else {
        resolve(response);
      }
    });
  });
};

let compressId = function(id, quality) {
  return new Promise((resolve, reject) => {
    request({
      method: 'POST',
      uri: `${host}compress-id?id=${id}&quality=${quality}`,
    }, function (error, response, body) {
      if (error) {
        reject(error);
      } else {
        resolve(response);
      }
    });
  });
};

let compressUrl = function(url, quality) {
  return new Promise((resolve, reject) => {
    request({
      method: 'POST',
      uri: `${host}compress-url?url=${encodeURIComponent(url)}&quality=${quality}`,
    }, function (error, response, body) {
      if (error) {
        reject(error);
      } else {
        resolve(response);
      }
    });
  });
};

let thumbnailBlob = function(blob, contentType, geometry) {
  return new Promise((resolve, reject) => {
    request({
      method: 'POST',
      uri: `${host}thumbnail-blob?geometry=${encodeURIComponent(geometry)}`,
      body: blob,
      headers: {
        'content-type': contentType
      }
    }, function (error, response, body) {
      if (error) {
        reject(error);
      } else {
        resolve(response);
      }
    });
  });
};

let thumbnailId = function(id, geometry) {
  return new Promise((resolve, reject) => {
    request({
      method: 'POST',
      uri: `${host}thumbnail-id?id=${id}&geometry=${encodeURIComponent(geometry)}`,
    }, function (error, response, body) {
      if (error) {
        reject(error);
      } else {
        resolve(response);
      }
    });
  });
};

let thumbnailUrl = function(url, geometry) {
  return new Promise((resolve, reject) => {
    request({
      method: 'POST',
      uri: `${host}thumbnail-url?url=${encodeURIComponent(url)}&geometry=${encodeURIComponent(geometry)}`,
    }, function (error, response, body) {
      if (error) {
        reject(error);
      } else {
        resolve(response);
      }
    });
  });
};

module.exports = function(config) {
  host = config.host;
  return {
    download: download,
    upload: upload,
    compressBlob: compressBlob,
    compressId: compressId,
    compressUrl: compressUrl,
    thumbnailBlob: thumbnailBlob,
    thumbnailId: thumbnailId,
    thumbnailUrl: thumbnailUrl
  };
};
